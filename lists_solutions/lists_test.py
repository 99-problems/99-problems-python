"""
Tests for lists
"""
import unittest

from lists_solutions.lists import s1_last, s2_second_last, s4_number_of_elements


class TestP01(unittest.TestCase):
    """
    Tests
    """

    def test_last_element(self):
        """Valid test of last element"""
        self.assertEqual(10, s1_last([1, 2, 3, 5, 10]))

    def test_bad_last_element(self):
        """Valid test of bad element"""
        self.assertNotEqual(1, s1_last([1, 2, 3, 5, 10]))

    def test_empty_last_element(self):
        """Valid test of empty list"""
        self.assertNotEqual(0, s1_last([]))


class Test02(unittest.TestCase):
    """Tests for s02"""

    def test_second_last_element(self):
        """Valid test of second element"""
        self.assertEqual(5, s2_second_last([1, 2, 3, 5, 10]))

    def test_bad_second_last_element(self):
        """Valid test of second element"""
        self.assertNotEqual(10, s2_second_last([1, 2, 3, 5, 10]))

    def test_empty_last_element(self):
        """Valid test of empty list"""
        self.assertNotEqual(0, s2_second_last([]))


class Test03(unittest.TestCase):
    pass


class Test04(unittest.TestCase):
    def test_list(self):
        self.assertEqual(5, s4_number_of_elements([1, 2, 3, 4, 5]))


if __name__ == '__main__':
    unittest.main()
