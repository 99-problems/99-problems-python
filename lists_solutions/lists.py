"""
Lists problems and solutions
"""


# [[first_last]]
def s1_last(arr):
    """
    Return the last element from array
    :param arr: input array
    :return: last item
    """
    if len(arr) > 0:
        return arr[-1]
    return AttributeError


# [[second_last]]
def s2_second_last(arr):
    """
    Return the second last element from array
    :param arr: input array
    :return: last item
    """
    if len(arr) > 1:
        return arr[-2]
    return ValueError("Empty list of invalid input")


# [[third]]
def s3_kth(arr, i):
    """
    Find the k-th item in last
    :param i: index of the item
    :param arr: input array
    :return: last item
    """
    if len(arr) > 0:
        return arr[i - 1]
    else:
        return ValueError("Empty list of invalid input")


def s4_number_of_elements(arr):
    """
    Find the number of elements of a list.
    :param arr: input array
    :return: number of elements of a list
    """
    return len(arr)
